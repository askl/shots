import React, { useEffect, useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

import config from '../../config';

const Scores = () => {
  const [message, setMessage] = useState();
  const [todayScores, setTodayScores] = useState();
  const [allTimeScores, setAllTimeScores] = useState();

  const loadData = () => {
    fetch(config.getScoresUrl, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    }).then(async res => {
      const data = await res.json();
      console.log(data);
      setAllTimeScores(data);
    }).catch(error => {
      console.log(error);
      setMessage({
        data: JSON.stringify(error),
        type: 'alert-danger',
      });
    });

    const todayISODate = new Date().toISOString();
    const todayISO = todayISODate.substring(0, todayISODate.indexOf('T'));

    fetch(config.getScoresUrl + new URLSearchParams({ 'date': todayISO }), {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    }).then(async res => {
      const data = await res.json();
      console.log(data);
      setTodayScores(data);
    }).catch(error => {
      console.log(error);
      setMessage({
        data: JSON.stringify(error),
        type: 'alert-danger',
      });
    });
  };

  useEffect(loadData, []);

  return (
    <div>
      <h2>Scores</h2>
      {message && (
        <div className='row'>
          <div
            className={`alert fade show d-flex ${message.type}`}
            role='alert'
          >
            {message.data}
            <span
              aria-hidden='true'
              className='ml-auto cursor-pointer'
              onClick={() => setMessage(null)}
            >
              &times;
            </span>
          </div>
        </div>
      )}
      <Tabs>
        <TabList>
          <Tab>Today</Tab>
          <Tab>All time</Tab>
        </TabList>
        <TabPanel>
          <table className="table">
            <thead>
              <tr>
                <th>User</th>
                <th>Score</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              {todayScores && todayScores.map(score => {
                return (
                  <tr key={score['date_created']}>
                    <td style={{ 'color': score['player']['color'] }}>{score['player']['user']['username']}</td>
                    <td>{score['player']['dollars']}</td>
                    <td>{score['date_created']}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </TabPanel>
        <TabPanel>
          <table className="table">
            <thead>
              <tr>
                <th>User</th>
                <th>Score</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              {allTimeScores && allTimeScores.map(score => {
                return (
                  <tr key={score['date_created']}>
                    <td style={{ 'color': score['player']['color'] }}>{score['player']['user']['username']}</td>
                    <td>{score['player']['dollars']}</td>
                    <td>{(new Date(score['date_created'])).toLocaleString('en-GB')}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </TabPanel>
      </Tabs>
    </div>
  );
}

export default Scores;