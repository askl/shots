import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import config from '../../config';

const Login = () => {
  const { register, handleSubmit, formState: {errors} } = useForm();
  const [message, setMessage] = useState();
  const history = useHistory();

  const onSubmit = (data, e) => {
    setMessage({
      data: 'Login is in progress...',
      type: 'alert-warning',
    });
    fetch(config.tokenAuthUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }).then(async res => {
      const data = await res.json();
      if(!res.ok) {
        setMessage({
          data: 'Invalid credentials!',
          type: 'alert-danger',
        });
        return Promise.reject(data);
      }
      setMessage({
        data: 'Login successful! Redirecting...',
        type: 'alert-success',
      });
      localStorage.setItem('token', data.token);
      history.push('/dashboard');
      e.target.reset();
    }).catch((error) => {
      const errorText = error.non_field_errors[0] || error;
      setMessage({
        data: `${errorText}`,
        type: 'alert-danger',
      });
    });
  };

  return (
    <div
      className={'container container-fluid d-flex align-items-center justify-content-center'}
    >
      <div className='loginFormContainer'>
        {message && (
          <div
            className={`alert fade show d-flex ${message.type}`}
            role='alert'
          >
            {message.data}
            <span
              aria-hidden='true'
              className='ml-auto cursor-pointer'
              onClick={() => setMessage(null)}
            >
              &times;
            </span>
          </div>
        )}
        <fieldset className='border p-3 rounded'>
          <legend
            className={'loginFormLegend border rounded p-1 text-center'}
          >
            Login
          </legend>
          <form onSubmit={handleSubmit(onSubmit)} noValidate autoComplete='off'>
            <div className='form-group'>
              <label htmlFor='username'>Username</label>
              <input
                id='username'
                name='username'
                type='text'
                className='form-control'
                aria-describedby='Enter your username'
                placeholder='Enter your username'
                {...register('username', {
                  required: {
                    value: true,
                    message: 'Please enter your username',
                  }
                })}
              />
              {errors.username && (
                <span className='errorMessage mandatory'>
                  {errors.username.message}
                </span>
              )}
            </div>
            <div className='form-group'>
              <label htmlFor='password'>Password</label>
              <input
                id='password'
                type='password'
                name='password'
                className='form-control'
                placeholder='Enter password'
                {...register('password', {
                  required: {
                    value: true,
                    message: 'Please enter password',
                  },
                })}
              />
              {errors.password && (
                <span className='errorMessage mandatory'>
                  {errors.password.message}
                </span>
              )}
            </div>
            <div className='d-flex align-items-center justify-content-center'>
              <button type='submit' className='btn btn-outline-dark'>
                Log in
              </button>
            </div>
          </form>
        </fieldset>
      </div>
    </div>
  );
};

export default Login;