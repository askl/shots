import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import config from '../../config';

const Dashboard = () => {
  const { register, handleSubmit } = useForm();
  const [message, setMessage] = useState();
  const history = useHistory();

  const colorRows = [
    ['blue', 'green', 'orange', 'pink'],
    ['purple', 'red', 'teal', 'yellow']
  ];

  const onSubmit = (data, e) => {
    setMessage({
      data: JSON.stringify(data),
      type: 'alert-warning',
    });

    console.log(data);

    fetch(config.joinGameUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Token ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(data)
    }).then(async res => {
      if (res.ok) {
        const data = await res.json();
        console.log(data);
        history.push(`/game/${data.id}`);
      } else {
        setMessage({
          data: JSON.stringify(res.statusText),
          type: 'alert-danger',
        });
      }
      e.target.reset();
    }).catch((error) => {
      setMessage({
        data: JSON.stringify(error),
        type: 'alert-danger',
      });
    });
  };

  return (
    <div className='container d-flex align-items-center justify-content-center'>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className='container-fluid'>
          {message && (
            <div className='row'>
              <div
                className={`alert fade show d-flex ${message.type}`}
                role='alert'
              >
                {message.data}
                <span
                  aria-hidden='true'
                  className='ml-auto cursor-pointer'
                  onClick={() => setMessage(null)}
                >
                  &times;
                </span>
              </div>
            </div>
          )}

          {colorRows.map((colorRow, idx) => (
            <div className='row' key={idx}>
              {colorRow.map(color => (
                <div className='col-md-3' key={color}>
                  <label>
                    <input type='radio' {...register('color')} value={color} />
                    <div className='icon'>
                      <img src={require(`../../img/players/${color}.svg`)} alt='' width='80px' height='80px' />
                    </div>
                  </label>
                </div>
              ))}
            </div>
          ))}
        </div>
        <div className='d-flex align-items-center justify-content-center'>
          <button type='submit' className='btn btn-outline-dark'>
            Find match
          </button>
        </div>
      </form>
    </div>
  );
}

export default Dashboard;