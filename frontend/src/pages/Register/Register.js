import React, { useState } from 'react';
import { useForm } from 'react-hook-form'
import config from '../../config';

import styles from '../../styles/index.css';

const Register = () => {
  const { register, handleSubmit, formState: {errors} } = useForm();
  const checkPasswords = () => document.getElementById('password').value === document.getElementById('passwordConfirmation').value;

  const [message, setMessage] = useState();
  const onSubmit = (data, e) => {
    delete data.passwordConfirmation;
    setMessage({
      data: 'Registration is in progress...',
      type: 'alert-warning'
    });
    fetch(config.registerUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    }).then(async res => {
      const data = await res.json();
      if(!res.ok) {
        setMessage({
          data: `${data}`,
          type: 'alert-danger',
        });
        return Promise.reject(data);
      }
      setMessage({
        data: 'Registration completed! Check your email for the confirmation code',
        type: 'alert-success',
      });
      e.target.reset();
    }).catch((error) => {
      setMessage({
        data: `${error}`,
        type: 'alert-danger',
      });
    });
  }

  return (
    <div className={`${styles.container} container-fluid d-flex align-items-center justify-content-center`}>
      <div className={styles.registrationFormContainer}>
        {message && (
          <div
            className={`alert fade show d-flex ${message.type}`}
            role='alert'
          >
            {message.data}
            <span
              aria-hidden='true'
              className='ml-auto cursor-pointer'
              onClick={() => setMessage(null)}
            >
              &times;
            </span>
          </div>
        )}
        <fieldset className='border p-3 rounded'>
          <legend className={`${styles.registrationFormLegend} border rounded p-1 text-center`}>
            Register
          </legend>
          <form onSubmit={handleSubmit(onSubmit)} noValidate autoComplete='off'>
            <div className='form-group'>
              <label htmlFor='email'>Email</label>
              <input
                id='email'
                name='email'
                type='email'
                className='form-control'
                aria-describedby='Enter email address'
                placeholder='Enter email address'
                {...register('email', {
                  required: {
                    value: true,
                    message: 'Please enter your email address',
                  },
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                    message: 'Enter a valid email address',
                  }
                })}
              />
              {errors.email && (
                <span className={`${styles.errorMessage} mandatory`}>
                  {errors.email.message}
                </span>
              )}
            </div>
            <div className='form-group'>
              <label htmlFor='username'>Username</label>
              <input
                id='username'
                name='username'
                type='text'
                className='form-control'
                aria-describedby='Enter your username'
                placeholder='Enter your username'
                {...register('username', {
                  required: {
                    value: true,
                    message: 'Please enter your username',
                  }
                })}
              />
              {errors.username && (
                <span className={`${styles.errorMessage} mandatory`}>
                  {errors.username.message}
                </span>
              )}
            </div>
            <div className='form-group'>
              <label htmlFor='password'>Password</label>
              <input
                id='password'
                type='password'
                className='form-control'
                placeholder='Enter password'
                {...register('password', {
                  required: {
                    value: true,
                    message: 'Please enter your password',
                  },
                  minLength : {
                    value : 6,
                    message : 'Password should have at least 6 characters'
                  }
                })}
              />
              {errors.password && (
                <span className={`${styles.errorMessage} mandatory`}>
                  {errors.password.message}
                </span>
              )}
            </div>
            <div className='form-group'>
              <label htmlFor='passwordConfirmation'>Confirm password</label>
              <input
                id='passwordConfirmation'
                type='password'
                className='form-control'
                placeholder='Confirm password'
                {...register('passwordConfirmation', {
                  required: {
                    value: true,
                    message: 'Please enter your password',
                  },
                  validate: () => checkPasswords() || 'Passwords do not match',
                  shouldUnregister: true
                })}
              />
              {errors.passwordConfirmation && (
                <span className={`${styles.errorMessage} mandatory`}>
                  {errors.passwordConfirmation.message}
                </span>
              )}
            </div>
            <div className='d-flex align-items-center justify-content-center'>
              <button type='submit' className='btn btn-outline-dark'>
                Submit
              </button>
            </div>
          </form>
        </fieldset>
      </div>
    </div>
  )
};

export default Register;