import React, { useEffect, useState, useRef } from 'react';
import { useForm } from 'react-hook-form';

import config from '../../config';
import ActionsPanel from '../../components/ActionsPanel';
import Board from '../../components/Board';
import StatsPanel from '../../components/StatsPanel';
import Timer from '../../components/Timer';
import WeaponsPanel from '../../components/WeaponsPanel';
import Equipment from '../../components/Equipment';
import Shop from '../../components/Shop';
import Logs from '../../components/Logs';

const Game = (params) => {
  const { register, handleSubmit } = useForm();

  const id = params.id;
  const [message, setMessage] = useState();
  const [timeCreated, setTimeCreated] = useState();
  const [turn, setTurn] = useState();
  const [boardData, setBoardData] = useState();
  const [shopItems, setShopItems] = useState();
  const [loggedInPlayer, setLoggedInPlayer] = useState();
  const [selectedTile, setSelectedTile] = useState();
  const [logs, setLogs] = useState();
  const [websocket, setWebsocket] = useState();
  const chatRef = useRef();

  const loadData = (data, ...setters) => {
    const [setTimeCreated, setTurn, setBoardData, setLoggedInPlayer] = setters;

    setTimeCreated(data.time_created);
    setTurn(data.turn);
    setLogs(data.logs);
    setShopItems(data.shop_items);

    let boardData = Array(4).fill().map(() => Array(4));
    data.players.forEach(player => {
      boardData[player.x][player.y] = { type: 'player', ...player }
      if (player.is_logged_in) setLoggedInPlayer(player);
    });

    data.banks.forEach(bank => boardData[bank.x][bank.y] = { type: 'bank', ...bank });

    setBoardData(boardData);

    console.log(chatRef);
    if (chatRef && chatRef.current) {
      chatRef.current.scrollIntoView({ behaviour: 'smooth' })
    }
  }

  const fetchData = (id, ...setters) => {
    fetch(config.postGameMoveUrl.replace('__ID__', id), {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Token ${localStorage.getItem('token')}`
      },
    }).then(async res => {
      const data = await res.json();
      console.log(data);
      // Locked means game is processed, repeat after short duration
      if (res.status === 423) {
        setTimeout(fetchData(id, ...setters), 5);
        return;
      }

      loadData(data, ...setters);
      setMessage(null);
    }).catch(error => {
      console.log(error);
      setMessage({
        data: JSON.stringify(error),
        type: 'alert-danger',
      });
    });
  }

  const onSubmit = (data, e) => {
    if (!data.position) {
      setMessage({
        data: 'No field selected!',
        type: 'alert-danger',
      });
      return;
    }
    const position = JSON.parse(data.position);
    data.x = position.x;
    data.y = position.y;
    delete data.position;
    console.log(data);
    fetch(config.postGameMoveUrl.replace('__ID__', id), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Token ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(data),
    }).then(async res => {
      setMessage({
        data: await res.text(),
        type: res.ok ? 'alert-success' : 'alert-danger',
      });
      e.target.reset();
    }).catch((error) => {
      setMessage({
        data: JSON.stringify(error),
        type: 'alert-danger',
      });
    });
  };

  useEffect(() => {
    fetchData(id, setTimeCreated, setTurn, setBoardData, setLoggedInPlayer);
    const websocket = new WebSocket(config.postGameMoveWsUrl
      .replace('__ID__', id)
      .replace('__TOKEN__', localStorage.getItem('token')));
    websocket.onopen = () => console.log('connected');
    websocket.onmessage = message => {
      console.log(message);
      loadData(JSON.parse(message.data), setTimeCreated, setTurn, setBoardData, setLoggedInPlayer);
    }
    websocket.onclose = () => console.log('disconnected');
    websocket.onerror = err => console.log(err);
    setWebsocket(websocket);
    // eslint-disable-next-line
  }, [id]);

  return (
    <form id='game'
      onSubmit={handleSubmit(onSubmit)}>
      <div className='row'>
        <div className='col-3'>
          {loggedInPlayer && <StatsPanel obj={loggedInPlayer} />}
          <Shop loggedInPlayer={loggedInPlayer} items={shopItems} websocket={websocket} />
          {loggedInPlayer && <Equipment items={loggedInPlayer.items} websocket={websocket} />}
        </div>
        <div className='col-6'>
          {timeCreated && turn !== undefined && (<Timer timeCreated={timeCreated} turn={turn} onTimeout={() =>
            fetchData(id, setTimeCreated, setTurn, setBoardData, setLoggedInPlayer)
          } />)}
          {boardData && <Board data={boardData} formRegister={register} onClick={(row, col) => setSelectedTile(boardData[row][col])} />}
          <div className='row'>
            <div className='col-6'>
              {loggedInPlayer && <WeaponsPanel weapons={loggedInPlayer.weapons} websocket={websocket} />}
            </div>
            <div className='col-6'>
              {loggedInPlayer && <ActionsPanel formRegister={register} />}
            </div>
            <br />
            <div className='text-center'>
              {loggedInPlayer && <button className='btn btn-outline-dark btn-block' onSubmit={() => null}>confirm</button>}
            </div>
          </div>
          {message && (
            <div className='row'>
              <div
                className={`alert fade show d-flex ${message.type}`}
                role='alert'
              >
                {message.data}
                <span
                  aria-hidden='true'
                  className='ml-auto cursor-pointer'
                  onClick={() => setMessage(null)}
                >
                  &times;
                </span>
              </div>
            </div>
          )}
        </div>
        <div className='col-3 justify-content-center'>
          {selectedTile && <StatsPanel obj={selectedTile} />}
          <div className='position-absolute h-40 w-25' style={{ bottom: '0px' }}>
            <Logs logs={logs} chatRef={chatRef} websocket={websocket} />
          </div>
        </div>
      </div>
    </form>
  );
};

export default Game;