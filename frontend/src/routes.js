import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import TopBar from './components/TopBar';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import Dashboard from './pages/Dashboard/Dashboard';
import Game from './pages/Game/Game';
import Scores from './pages/Scores/Scores';
import NotFound from './pages/NotFound/NotFound'

export const Routes = (props) => {
  return (
    <BrowserRouter {...props}>
      <TopBar />
      <Switch>
        <Route path='/login' component={Login} />
        <Route path='/register' component={Register} />
        <Route path='/dashboard' render={() => localStorage.getItem('token') ? (<Dashboard />) : (<Redirect to='login' />)} />
        <Route path='/game/:id' render={({ match }) => (<Game id={match.params.id} />)} />
        <Route path='/scores' component={Scores} />
        <Route exact path='/'>
          <Redirect to='/dashboard' />
        </Route>
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
};