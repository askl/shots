const apiUrl = 'http://127.0.0.1:8000/api/';
const apiWsUrl = 'ws://127.0.0.1:8000/ws/';
// const apiUrl = 'http://backend:8000/api/';
const config = {
    apiUrl: apiUrl,
    registerUrl: apiUrl + 'register/',
    tokenAuthUrl: apiUrl + 'token-auth/',
    joinGameUrl: apiUrl + 'game/join/',
    getGameStateUrl: apiUrl + 'game/__ID__/',
    postGameMoveUrl: apiUrl + 'game/__ID__/',
    postGameMoveWsUrl: apiWsUrl + 'game/__ID__/?token=__TOKEN__',
    getScoresUrl: apiUrl + 'scores/?',
}

export default config;