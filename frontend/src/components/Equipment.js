import React from 'react';

const handleUseItem = (websocket, item) => {
  websocket.send(JSON.stringify({
    "type": "use",
    "value": item.type
  }));
};

const Equipment = props => {
  const { items, websocket } = props;

  return (
    <table className='table table-bordered'>
      <thead>
        <tr>
          <th className='text-center' colSpan={3}>
            Equipment
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          {[0, 1, 2].map(idx => {
            return (
              <td className='col-4 text-center' key={Math.random()}>
                {items[idx] && (
                  <button className='btn btn-outline-dark'
                    onClick={e => {
                      e.preventDefault();
                      handleUseItem(websocket, items[idx]);
                    }}>
                    <img src={require(`../img/items/${items[idx].type}.svg`)} alt='' width='40px' height='40px' />
                    <span>{items[idx].type}</span>
                  </button>
                )}
              </td>
            );
          })}
        </tr>
      </tbody>
    </table >
  );
};

export default Equipment;