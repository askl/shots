import React from 'react';

const renderObject = object => {
  if (object) {
    if (object.type === 'player') {
      return (
        <div className='icon'>
          <img src={require(`../img/players/${object.color}.svg`)} alt='' width='80px' height='80px' style={object.is_logged_in ? { border: '3px solid lightgreen' } : {}} />
        </div>
      );
    } else if (object.type === 'bank') {
      return (
        <div className='icon'>
          <img src={require('../img/objects/bank.svg').default} alt='' width='80px' height='80px' />
        </div>
      );
    }
  }
  return (<div className='icon'></div>);
}

const Board = props => {
  const { data, formRegister, onClick } = props;

  return (
    <table className='table table-bordered d-flex justify-content-center'>
      <tbody>
        {
          [0, 1, 2, 3].map(row => {
            return (
              <tr key={`row-${row}`}>
                {
                  [0, 1, 2, 3].map(col => {
                    return (
                      <td key={`cell-${row}-${col}`} className='cell' onClick={() => onClick(row, col)}>
                        <label>
                          <input type='radio'
                            defaultChecked={!!(data[row][col] && data[row][col].is_logged_in)}
                            {...formRegister('position', { required: true })}
                            value={`{"x":${row},"y":${col}}`}
                          />
                          {renderObject(data[row][col])}
                        </label>
                      </td>
                    );
                  })
                }
              </tr>
            );
          })
        }
      </tbody>
    </table>
  );
};

export default Board;