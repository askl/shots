import React, { useState, useEffect } from 'react';

const Timer = props => {
  const { timeCreated, turn, onTimeout } = props;
  const [timeLeft, setTimeLeft] = useState();

  useEffect(() => {
    let timerEnd = new Date(Date.parse(timeCreated) + (turn * 11 * 1000));
    setTimeLeft(new Date(timerEnd - new Date()).getSeconds());

    const timeoutId = setTimeout(onTimeout, timerEnd - new Date());

    const intervalId = setInterval(() => {
      setTimeLeft(new Date(timerEnd - new Date()).getSeconds());
    }, 1000);

    return () => {
      clearInterval(intervalId);
      clearTimeout(timeoutId);
    };
    // eslint-disable-next-line
  }, [turn]);

  return (
    <div className='containter-flex d-flex justify-content-center border-2'>
      {timeLeft}
    </div>
  );
};

export default Timer;