import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';

const TopBar = () => {
  const [loggedIn, setLoggedIn] = useState(!!localStorage.getItem('token'));
  const history = useHistory()

  useEffect(() => {
    history.listen(() => setLoggedIn(!!localStorage.getItem('token')))
  }, [history]);

  const logOut = () => {
    localStorage.clear();
  }

  return (
    <nav className='nav nav-pills justify-content-end'>
      {!loggedIn && (<div className='d-flex'>
        <Link to='/login' className='nav-item nav-link'>Log in</Link>
        <Link to='/register' className='nav-item nav-link'>Register</Link>
        <Link to='/scores' className='nav-item nav-link'>High scores</Link>
      </div>)}
      {loggedIn && (<div className='d-flex'>
        <Link to='/dashboard' className='nav-item nav-link'>Play</Link>
        <Link to='/scores' className='nav-item nav-link'>High scores</Link>
        <Link to='/login' className='nav-item nav-link' onClick={() => logOut()}>Log out</Link>
      </div>)}
    </nav>
  );
}

export default TopBar;