import React from 'react';

const handleBuyItem = (websocket, item, loggedInPlayer) => {
  if (loggedInPlayer) {
    websocket.send(JSON.stringify({
      "type": "buy",
      "value": item
    }));
  }
};

const handleSkipItem = (websocket, item, loggedInPlayer) => {
  if (loggedInPlayer) {
    websocket.send(JSON.stringify({
      "type": "skip",
      "value": item
    }));
  }
};

const Shop = props => {
  const { loggedInPlayer, items, websocket } = props;

  return (
    <table className='table table-bordered'>
      <thead>
        <tr>
          <th className='text-center' colSpan={2}>
            Shop
          </th>
        </tr>
      </thead>
      <tbody>
        {items && items.map(item => {
          return (
            <tr key={Math.random()}>
              <td className='d-flex justify-content-between'>
                <span>
                  <img src={require(`../img/items/${item.item}.svg`)} alt='' width='40px' height='40px' />
                  {item.item}
                </span>
                <span>
                  <button className='btn btn-outline-danger' onClick={e => {
                    e.preventDefault();
                    handleSkipItem(websocket, item.item, loggedInPlayer);
                  }}>skip 20$</button>
                </span>
              </td>
              <td className='text-center'>
                <button className='btn btn-outline-dark' onClick={e => {
                  e.preventDefault();
                  handleBuyItem(websocket, item.item, loggedInPlayer);
                }}>{item.price}$</button>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  );
};

export default Shop;