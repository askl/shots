import React from 'react';

const ActionsPanel = props => {
  const { formRegister } = props;

  return (
    <div className='text-start'>
      <label>
        <input type='radio' defaultChecked='true' {...formRegister('type', { required: true })} value='hide' />
        <div className='smallicon'>
          <img src={require('../img/actions/hide.svg').default} alt='' width='40px' height='40px' />
        </div>
        <span>Hide</span>
      </label>
      <br />
      <label>
        <input type='radio' {...formRegister('type', { required: true })} value='move' />
        <div className='smallicon'>
          <img src={require('../img/actions/move.svg').default} alt='' width='40px' height='40px' />
        </div>
        <span>Move</span>
      </label>
      <br />
      <label>
        <input type='radio' {...formRegister('type', { required: true })} value='reload' />
        <div className='smallicon'>
          <img src={require('../img/actions/reload.svg').default} alt='' width='40px' height='40px' />
        </div>
        <span>Reload</span>
      </label>
      <br />
      <label>
        <input type='radio' {...formRegister('type', { required: true })} value='shoot' />
        <div className='smallicon'>
          <img src={require('../img/actions/shoot.svg').default} alt='' width='40px' height='40px' />
        </div>
        <span>Shoot</span>
      </label>
    </div>
  );
};

export default ActionsPanel;