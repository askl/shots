import React from 'react';

const handleWeaponSwitch = (websocket, type) => {
  websocket.send(JSON.stringify({
    "type": "switch",
    "value": type
  }));
}

const WeaponsPanel = props => {
  const { weapons, websocket } = props;

  return (
    <div className='text-end'>
      {weapons && weapons.map(weapon => {
        return (
          <div key={weapon.type}>
            <label onClick={() => handleWeaponSwitch(websocket, weapon.type)}>
              <input type='radio' value={weapon.type} defaultChecked={weapon.is_equiped} name='weapon' />
              <div className='smallicon'>
                <img src={require(`../img/items/${weapon.type}.svg`)} alt='' width='40px' height='40px' />
              </div>
              <span>{weapon.type} {weapon.ammo}/{weapon.max_ammo}</span>
            </label>
          </div>
        );
      })}
    </div>
  );
};

export default WeaponsPanel;