import React from 'react';

const DISPLAYED_PROPERTIES = ['user', 'type', 'hp', 'dollars', 'items', 'weapons'];

const mapToString = (prop) => {
  if (typeof prop === "object") {
    // is user
    if (prop.username) return prop.username;
    // is item
    else return (prop.map(item => {
      if (item.ammo === undefined) {
        return item.type;
      } else {
        return `${item.is_equiped ? '•' : ' '}${item.type} ${item.ammo}/${item.max_ammo}`
      }
    }))//.reduce((a, b) => a + '\n' + b))
  } else {
    return prop;
  }
};

const StatsPanel = props => {
  const obj = props.obj;
  return (
    <table className='table table-bordered mh-50'>
      <tbody>
        {obj && Object.keys(obj)
          .filter(key => DISPLAYED_PROPERTIES.includes(key))
          .map(key => (
            <tr key={key}>
              <td>{key}</td>
              <td>{mapToString(obj[key])}</td>
            </tr>
          ))
        }
      </tbody>
    </table>
  );
};

export default StatsPanel;