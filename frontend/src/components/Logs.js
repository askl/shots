import React from 'react';

const sendMessage = (message, websocket) => {
  websocket.send(JSON.stringify({
    'type': 'chat',
    'value': message
  }));
};

const Logs = props => {
  const { logs, chatRef, websocket } = props;
  console.log(chatRef);

  return (
    <div className='logs' >
      {logs && logs.map(log => {
        const date = new Date(Date.parse(log.date_created));
        return (
          <p key={log.date_created}><b>[{date.toLocaleTimeString('en-GB')}]</b> {log.message}</p>
        );
      })}
      <input type='text' className='form-text' placeholder='chat' onKeyDown={e => {
        if (e.key === 'Enter') {
          e.preventDefault();
          sendMessage(e.target.value, websocket);
          e.target.value = '';
        }
      }} ref={chatRef} />
    </div>
  );
};

export default Logs;