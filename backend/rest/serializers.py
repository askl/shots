from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest.models import (
    RegistrationForm,
    Player,
    Game,
    Bank,
    Item,
    Weapon,
    Action,
    ShopItem,
    LogEntry,
    Score,
)


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ("username",)


class RegistrationFormSerializer(ModelSerializer):
    class Meta:
        model = RegistrationForm
        fields = (
            "username",
            "email",
            "password",
            "confirmation_code",
            "valid_until",
            "confirmed",
        )


class ItemSerializer(ModelSerializer):
    class Meta:
        model = Item
        fields = ["type"]


class WeaponSerializer(ModelSerializer):
    class Meta:
        model = Weapon
        fields = ("type", "ammo", "max_ammo", "is_equiped")


class PlayerSerializer(ModelSerializer):
    class Meta:
        model = Player
        fields = (
            "user",
            "x",
            "y",
            "color",
            "hp",
            "dollars",
            "items",
            "weapons",
        )

    user = UserSerializer(read_only=True)
    items = ItemSerializer(many=True, read_only=True)
    weapons = WeaponSerializer(many=True, read_only=True)

    def to_representation(self, instance):
        repr = super().to_representation(instance)
        if self.context["request"]:
            repr["is_logged_in"] = instance.user == self.context["request"].user
        return repr


class BankSerializer(ModelSerializer):
    class Meta:
        model = Bank
        fields = ("x", "y", "hp")


class ShopItemSerializer(ModelSerializer):
    class Meta:
        model = ShopItem
        fields = ("price", "item", "type")


class LogSerializer(ModelSerializer):
    class Meta:
        model = LogEntry
        fields = ("date_created", "message")


class GameSerializer(ModelSerializer):
    class Meta:
        model = Game
        fields = (
            "id",
            "time_created",
            "turn",
            "players",
            "banks",
            "shop_items",
            "logs",
        )

    players = PlayerSerializer(many=True, read_only=True)
    banks = BankSerializer(many=True, read_only=True)
    shop_items = ShopItemSerializer(many=True, read_only=True)
    logs = SerializerMethodField()

    def get_logs(self, obj):
        return list(
            map(lambda a: LogSerializer(a).data, obj.logs.order_by("date_created")[:30])
        )


class ActionSerializer(ModelSerializer):
    class Meta:
        model = Action
        fields = ("type", "turn", "time", "x", "y")


class UserPlayerSerializer(ModelSerializer):
    class Meta:
        model = Player
        fields = ("user", "color", "dollars")

    user = UserSerializer(read_only=True)


class ScoreSerializer(ModelSerializer):
    class Meta:
        model = Score
        fields = ("player", "date_created")

    player = UserPlayerSerializer(read_only=True)
