from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from rest.views import registerUser, confirmRegistration, joinGame, GameView, ScoreView

urlpatterns = [
    path("register/", registerUser),
    path("register/confirm/", confirmRegistration),
    path("token-auth/", obtain_auth_token),
    path("game/join/", joinGame),
    path("game/<pk>/", GameView.as_view()),
    path("scores/", ScoreView.as_view()),
]
