import random
from rest.models import Weapon, ShopItem


class WeaponFactory:
    WEAPON_MAX_AMMO = {
        "revolver": 6,
        "shotgun": 2,
        "rifle": 1,
    }

    def getWeapon(self, type):
        return Weapon(type=type, max_ammo=WeaponFactory.WEAPON_MAX_AMMO.get(type))


class ShopItemFactory:
    ITEM_TYPES = {
        "bandage": "item",
        "shotgun": "weapon",
        "rifle": "weapon",
        "horse": "item",
    }
    ITEM_PRICES = {"bandage": 10, "shotgun": 30, "rifle": 50, "horse": 100}
    ITEM_PROBABILITIES = {"bandage": 5, "shotgun": 2, "rifle": 2, "horse": 1}

    def getRandomShopItems(self, k):
        items = random.choices(
            list(ShopItemFactory.ITEM_PROBABILITIES.keys()),
            weights=ShopItemFactory.ITEM_PROBABILITIES.values(),
            k=k,
        )

        return [self.getShopItem(item) for item in items]

    def getShopItem(self, item):
        return ShopItem(
            item=item,
            type=ShopItemFactory.ITEM_TYPES.get(item),
            price=ShopItemFactory.ITEM_PRICES.get(item),
        )
