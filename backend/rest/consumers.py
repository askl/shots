import json
from datetime import timedelta

from asgiref.sync import async_to_sync
from channels.db import database_sync_to_async
from channels.generic.websocket import JsonWebsocketConsumer
from django.db.models import (
    F,
    ExpressionWrapper,
    DateTimeField,
)
from django.utils import timezone

from rest.factories import WeaponFactory, ShopItemFactory
from rest.models import Game, Weapon, Item, LogEntry, Score
from rest.serializers import GameSerializer


class ActionConsumer(JsonWebsocketConsumer):
    def connect(self):
        self.user = self.scope["user"]
        self.game_id = self.scope["url_route"]["kwargs"]["pk"]
        self.group_name = f"game_{self.game_id}"
        self.weapon_factory = WeaponFactory()
        self.item_factory = ShopItemFactory()

        async_to_sync(self.channel_layer.group_add)(self.group_name, self.channel_name)

        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name, self.channel_name
        )

    def receive_json(self, content, **kwargs):
        active_game = Game.objects.get(pk=self.game_id)
        if content["type"] == "buy":
            skipped_item = active_game.shop_items.filter(item=content["value"]).first()
            player = active_game.players.filter(user=self.user).first()
            if player.dollars >= skipped_item.price:
                if skipped_item.type == "item":
                    if player.items.count() < 3:
                        Item(player=player, type=skipped_item.item).save()
                        LogEntry(
                            game=active_game,
                            message=f"{player.color} bought {skipped_item.item}",
                        ).save()
                    else:
                        return
                elif skipped_item.type == "weapon":
                    if not player.weapons.filter(type=skipped_item.item).first():
                        bought_weapon = self.weapon_factory.getWeapon(skipped_item.item)
                        bought_weapon.player = player
                        bought_weapon.save()
                        LogEntry(
                            game=active_game,
                            message=f"{player.color} bought {skipped_item.item}",
                        ).save()
                    else:
                        return
                player.dollars -= skipped_item.price
                player.save(update_fields=["dollars"])
                skipped_item.delete()
                [new_shop_item] = self.item_factory.getRandomShopItems(1)
                new_shop_item.game = active_game
                new_shop_item.save()

        elif content["type"] == "use":
            player = active_game.players.filter(user=self.user).first()
            item = player.items.filter(type=content["value"]).first()
            if item:
                if item.type == "bandage" and player.hp < 5:
                    player.hp += 1
                    player.save(update_fields=["hp"])
                    LogEntry(
                        game=active_game,
                        message=f"{player.color} used a {item.type}",
                    ).save()
                elif item.type == "horse":
                    player.game = None
                    player.save()
                    Score(player=player).save()
                    LogEntry(
                        game=active_game,
                        message=f"{player.color} galloped to the sunset with {player.dollars}$",
                    ).save()
                item.delete()

        elif content["type"] == "skip":
            skipped_item = active_game.shop_items.filter(item=content["value"]).first()
            player = active_game.players.filter(user=self.user).first()
            if player.dollars >= 20:
                player.dollars -= 20
                player.save(update_fields=["dollars"])
                skipped_item.delete()
                LogEntry(
                    game=active_game,
                    message=f"{player.color} skipped {skipped_item.item}",
                ).save()
                [new_shop_item] = self.item_factory.getRandomShopItems(1)
                new_shop_item.game = active_game
                new_shop_item.save()

        elif content["type"] == "switch":
            player = active_game.players.filter(user=self.user).first()
            selected_weapon = player.weapons.filter(type=content["value"]).first()
            if selected_weapon:
                player.weapons.update(is_equiped=False)
                selected_weapon.is_equiped = True
                selected_weapon.save()

        elif content["type"] == "chat":
            player = active_game.players.filter(user=self.user).first()
            LogEntry(
                game=active_game, message=f"{player.color}: {content['value']}"
            ).save()

        async_to_sync(self.channel_layer.group_send)(
            self.group_name, {"type": "game_update", "id": self.game_id}
        )

    def game_update(self, event):
        game_id = event["id"]
        serializer = GameSerializer(Game.objects.get(pk=self.game_id))
        # trick to have user in context
        serializer.context["request"] = lambda: None
        setattr(serializer.context["request"], "user", self.user)
        self.send_json(serializer.data)
