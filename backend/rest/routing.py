from django.urls import path

from .consumers import ActionConsumer

websocket_urlpatterns = [
    path('ws/game/<pk>/', ActionConsumer.as_asgi()),
]