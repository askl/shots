from django.db.models import (
    Model,
    DateTimeField,
    CharField,
    BooleanField,
    SmallIntegerField,
    IntegerField,
    ForeignKey,
    ManyToManyField,
    OneToOneField,
    CASCADE,
)
from django.contrib.auth.models import User
import datetime


class SerializableDateTimeField(DateTimeField):
    def value_to_string(self, obj):
        val = self.value_from_object(obj)
        if val:
            return val.isoformat()
        return ""


class RegistrationForm(Model):
    username = CharField(max_length=20)
    email = CharField(max_length=30)
    password = CharField(max_length=20)
    confirmation_code = CharField(max_length=50)
    valid_until = SerializableDateTimeField()
    confirmed = BooleanField(default=False)


class Game(Model):
    time_created = SerializableDateTimeField(auto_now_add=True)
    turn = IntegerField(default=0)


class Player(Model):
    PLAYER_COLORS = (
        ("blue", "blue"),
        ("green", "green"),
        ("orange", "orange"),
        ("pink", "pink"),
        ("purple", "purple"),
        ("red", "red"),
        ("teal", "teal"),
        ("yellow", "yellow"),
    )
    user = ForeignKey(User, on_delete=CASCADE, related_name="players")
    game = ForeignKey(Game, on_delete=CASCADE, null=True, related_name="players")
    x = SmallIntegerField()
    y = SmallIntegerField()
    color = CharField(max_length=20, choices=PLAYER_COLORS)
    hp = SmallIntegerField(default=5)
    dollars = SmallIntegerField(default=0)
    hidden = BooleanField(default=False)


class Bank(Model):
    game = ForeignKey(Game, on_delete=CASCADE, related_name="banks")
    x = SmallIntegerField()
    y = SmallIntegerField()
    hp = SmallIntegerField(default=500)


class Item(Model):
    ITEM_TYPES = (("bandage", "bandage"), ("horse", "horse"))

    player = ForeignKey(Player, on_delete=CASCADE, related_name="items")
    type = CharField(max_length=20, choices=ITEM_TYPES)


class Weapon(Model):
    WEAPON_TYPES = (
        ("revolver", "revolver"),
        ("shotgun", "shotgun"),
        ("rifle", "rifle"),
    )

    player = ForeignKey(Player, on_delete=CASCADE, related_name="weapons")
    type = CharField(max_length=20, choices=WEAPON_TYPES)
    ammo = SmallIntegerField(default=0)
    max_ammo = SmallIntegerField()
    is_equiped = BooleanField(default=False)


class Action(Model):
    ACTION_TYPES = (
        ("hide", "hide"),
        ("move", "move"),
        ("reload", "reload"),
        ("shoot", "shoot"),
    )

    game = ForeignKey(Game, on_delete=CASCADE, related_name="actions")
    player = ForeignKey(Player, on_delete=CASCADE, related_name="actions")
    type = CharField(max_length=20, choices=ACTION_TYPES)
    turn = IntegerField()
    time = SerializableDateTimeField(auto_now_add=True)
    x = SmallIntegerField()
    y = SmallIntegerField()


class ShopItem(Model):
    ITEM_TYPES = Weapon.WEAPON_TYPES + Item.ITEM_TYPES

    game = ForeignKey(Game, on_delete=CASCADE, related_name="shop_items")
    price = SmallIntegerField()
    item = CharField(max_length=20, choices=ITEM_TYPES)
    type = CharField(max_length=20)


class LogEntry(Model):
    game = ForeignKey(Game, on_delete=CASCADE, related_name="logs")
    date_created = SerializableDateTimeField(auto_now_add=True)
    message = CharField(max_length=500)


class Score(Model):
    player = ForeignKey(Player, on_delete=CASCADE, related_name="scores")
    date_created = SerializableDateTimeField(auto_now_add=True)
