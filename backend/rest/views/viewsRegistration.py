from datetime import timedelta
from secrets import token_hex

from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.http.response import JsonResponse
from django.utils import timezone

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from rest.models import RegistrationForm
from rest.serializers import RegistrationFormSerializer


@api_view(["POST"])
def registerUser(request):
    request_obj = JSONParser().parse(request)
    request_obj["confirmation_code"] = token_hex(15)
    request_obj["valid_until"] = timezone.now() + timedelta(minutes=20)
    request_obj["confirmed"] = False

    serializer = RegistrationFormSerializer(data=request_obj)
    if serializer.is_valid():
        if checkIfUserWithAttributeExists(email=request_obj["email"]):
            return Response(
                "User with this email already exists or is waiting for confirmation!",
                status=status.HTTP_400_BAD_REQUEST,
            )

        if checkIfUserWithAttributeExists(username=request_obj["username"]):
            return Response(
                "User with this username already exists or is waiting for confirmation!",
                status=status.HTTP_400_BAD_REQUEST,
            )
        serializer.save()
        send_mail(
            "Confirmation code",
            f'Click on the following link to complete registration: {request.build_absolute_uri()}confirm?confirmation_code={request_obj["confirmation_code"]}',
            "pro3b@pja.edu",
            [request_obj["email"]],
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def confirmRegistration(request):
    code = request.GET.get("confirmation_code", None)

    form = RegistrationForm.objects.filter(
        confirmation_code=code, valid_until__gte=timezone.now(), confirmed=False
    ).first()

    if form != None:
        User.objects.create_user(
            username=form.username, email=form.email, password=form.password
        )
        form.confirmed = True
        form.save()

        return Response("Success!", status=status.HTTP_200_OK)
    return Response("Bad parameters!", status=status.HTTP_400_BAD_REQUEST)


def checkIfUserWithAttributeExists(**attrs):
    existing_user = User.objects.filter(**attrs).first()
    existing_form = (
        RegistrationForm.objects.filter(**attrs)
        .filter(valid_until__gte=timezone.now(), confirmed=False)
        .first()
    )
    return existing_user != None or existing_form != None
