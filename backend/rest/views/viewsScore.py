from datetime import date
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from rest.models import Score
from rest.serializers import ScoreSerializer


class ScoreView(ListAPIView):
    queryset = Score.objects.all().order_by("-player__dollars")
    serializer_class = ScoreSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        if request.query_params.get("date"):
            filter_date = date.fromisoformat(request.query_params.get("date"))
            self.queryset = self.queryset.filter(date_created__date=filter_date)
        self.queryset = self.queryset[:10]
        return super().get(self, request, *args, **kwargs)
