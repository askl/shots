from datetime import timedelta
from random import choice

from django.db.models import (
    Count,
    F,
    ExpressionWrapper,
    DateTimeField,
    IntegerField,
)
from django.db.models.expressions import CombinedExpression
from django.utils import timezone

from lock_tokens.sessions import check_for_session, lock_for_session, unlock_for_session
from lock_tokens.exceptions import AlreadyLockedError

from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import RetrieveAPIView

from rest.factories import ShopItemFactory
from rest.models import Game, Bank, Player, Weapon, Item, Action, LogEntry, ShopItem
from rest.serializers import GameSerializer, ActionSerializer


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def joinGame(request):
    # If user already in a game return it
    active_game = (
        Game.objects.annotate(
            expiration_time=ExpressionWrapper(
                F("time_created") + (F("turn") + 1) * timedelta(seconds=11),
                output_field=DateTimeField(),
            )
        )
        .filter(
            players__user=request.user,
            expiration_time__gt=timezone.now(),
        )
        .first()
    )

    if active_game is not None:
        serializer = GameSerializer(active_game, context={"request": request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    player_color = request.data["color"]
    if player_color is None:
        player_color = choice(Player.PLAYER_COLORS[0])

    available_games = (
        Game.objects.alias(count_players=Count("players__id"))
        .annotate(
            expiration_time=ExpressionWrapper(
                F("time_created") + (F("turn") + 1) * timedelta(seconds=11),
                output_field=DateTimeField(),
            )
        )
        .filter(count_players__lt=4, expiration_time__gt=timezone.now())
        .exclude(players__color=player_color)
    )

    game = None

    if len(available_games) != 0:
        game = choice(available_games)
    else:
        game = Game.objects.create(time_created=timezone.now(), turn=1)
        game.save()

        LogEntry(game=game, message="Game created").save()

        freeSpace = choice(getFreeSpaces(game))
        Bank(x=freeSpace[0], y=freeSpace[1], game=game).save()

        factory = ShopItemFactory()
        shop_items = factory.getRandomShopItems(3)
        for item in shop_items:
            item.game = game
            item.save()

    freeSpace = choice(getFreeSpaces(game))
    player = Player(
        user=request.user,
        game=game,
        color=player_color,
        x=freeSpace[0],
        y=freeSpace[1],
        dollars=1000,
    )
    player.save()
    Weapon(type="revolver", player=player, is_equiped=True, max_ammo=6).save()
    Item(type="bandage", player=player).save()

    LogEntry(game=game, message=f"{player_color} joined the game").save()

    serializer = GameSerializer(game, context={"request": request})
    return Response(serializer.data, status=status.HTTP_200_OK)


class GameView(RetrieveAPIView):
    queryset = Game.objects.alias(
        expiration_time=ExpressionWrapper(
            F("time_created") + (F("turn") + 1) * timedelta(seconds=11),
            output_field=DateTimeField(),
        )
    ).filter(expiration_time__gt=timezone.now())
    # queryset = Game.objects.all()
    serializer_class = GameSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        game = Game.objects.get(id=kwargs["pk"])
        if (game.time_created + timedelta(seconds=11 * game.turn)) > timezone.now():
            pass
        elif (
            game.time_created + timedelta(seconds=11 * (game.turn + 1))
        ) > timezone.now():
            try:
                lock_for_session(game, request.session)
                finishTurn(game)
                unlock_for_session(game, request.session)
            except AlreadyLockedError:
                return Response(
                    data="Turn being processed, try again!",
                    status=status.HTTP_423_LOCKED,
                )

        return super().get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        game = Game.objects.get(id=kwargs["pk"])
        player = Player.objects.filter(user=request.user, game=game).first()
        actionForm = JSONParser().parse(request)
        actionForm["turn"] = game.turn

        serializer = ActionSerializer(data=actionForm)
        if serializer.is_valid():
            action = Action(**serializer.validated_data)

            selected_weapon = player.weapons.filter(is_equiped=True).first()
            if not selected_weapon:
                return Response(
                    "No weapon selected!", status=status.HTTP_400_BAD_REQUEST
                )

            is_possible, error_msg = checkIfActionPossible(action, game, player)
            if not is_possible:
                return Response(error_msg, status=status.HTTP_400_BAD_REQUEST)

            previous_action = Action.objects.filter(
                player=player, game=game, turn=game.turn
            ).first()
            if previous_action:
                previous_action.delete()

            serializer.save(player=player, game=game)
            return Response("Move submitted.", status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def getFreeSpaces(game):
    freeSpaces = {(x, y) for x in range(0, 4) for y in range(0, 4)}
    freeSpaces = freeSpaces - {(player.x, player.y) for player in game.players.all()}
    freeSpaces = freeSpaces - {(bank.x, bank.y) for bank in game.banks.all()}
    return list(freeSpaces)


def checkIfActionPossible(action, game, player):
    if action.type == "hide":
        pass
    elif action.type == "move":
        if getDistance(objToPoint(player), objToPoint(action)) > 1:
            return False, "Target field too far!"
        elif (action.x, action.y) not in getFreeSpaces(game):
            return False, "Target field already occupied!"
    elif action.type == "reload":
        weapon = player.weapons.get(is_equiped=True)
        if weapon.ammo >= weapon.max_ammo:
            return False, "Cannot load more ammo!"
    elif action.type == "shoot":
        if action.x == player.x and action.y == player.y:
            return False, "Cannot shoot yourself!"

        weapon = player.weapons.get(is_equiped=True)
        if not (weapon.ammo > 0):
            return False, "No ammo!"

        if weapon.type == "revolver":
            if not (action.x == player.x or action.y == player.y):
                return False, "Target not in a straight line!"
        elif weapon.type == "shotgun":
            if getDistance(objToPoint(player), objToPoint(action)) > 1:
                return False, "Target too far!"
        elif weapon.type == "rifle":
            pass
    return True, ""


def getDistance(point1, point2):
    return ((point2[0] - point1[0]) ** 2 + (point2[1] - point1[1]) ** 2) ** (1 / 2)


def objToPoint(obj):
    return (obj.x, obj.y)


def finishTurn(game):
    actions = Action.objects.filter(game=game, turn=game.turn).order_by("type", "time")
    for action in actions:
        resolveAction(action, game)

    for dead_player in game.players.filter(hp=0):
        # TODO: status
        LogEntry(game=game, message=f"{dead_player.color} died").save()
        dead_player.delete()

    for destroyed_bank in game.banks.filter(hp=0):
        LogEntry(game=game, message=f"{dead_player.color} was destroyed").save()
        destroyed_bank.delete()

    game.players.update(hidden=False)
    game.turn += 1
    game.save()


def resolveAction(action, game):
    if action.type == "hide":
        action.player.hidden = True
        action.player.save()
        LogEntry(game=game, message=f"{action.player.color} has hidden").save()
    elif action.type == "move":
        if (action.x, action.y) in getFreeSpaces(game):
            action.player.x = action.x
            action.player.y = action.y
            action.player.save()
            LogEntry(
                game=game,
                message=f"{action.player.color} moved to {action.x} {action.y}",
            ).save()
        else:
            LogEntry(
                game=game,
                message=f"{action.player.color} couldn't move to {action.x} {action.y}",
            ).save()
    elif action.type == "reload":
        weapon = action.player.weapons.get(is_equiped=True)
        if weapon.ammo < weapon.max_ammo:
            weapon.ammo += 1
            weapon.save()
            LogEntry(
                game=game,
                message=f"{action.player.color} reloaded his {weapon.type}",
            ).save()
        else:
            LogEntry(
                game=game,
                message=f"{action.player.color} tried to reload his {weapon.type} but its already full",
            ).save()
    elif action.type == "shoot":
        weapon = action.player.weapons.get(is_equiped=True)
        if not (weapon.ammo > 0):
            LogEntry(
                game=game,
                message=f"{action.player.color}'s tried to shoot but had no ammo",
            ).save()
            return
        weapon.ammo -= 1
        weapon.save()
        targets = ()

        if weapon.type == "revolver":
            if action.player.x == action.x and action.player.y < action.y:
                targets = {(action.x, y) for y in range(action.player.y + 1, 4)}
            elif action.player.x == action.x and action.player.y > action.y:
                targets = {(action.x, y) for y in range(action.player.y - 1, -1, -1)}
            elif action.player.x < action.x and action.player.y == action.y:
                targets = {(x, action.y) for x in range(action.player.x + 1, 4)}
            elif action.player.x > action.x and action.player.y == action.y:
                targets = {(x, action.y) for x in range(action.player.x - 1, -1, -1)}
        elif weapon.type == "shotgun":
            targets = set(
                filter(
                    lambda point: getDistance(objToPoint(action.player), point) <= 1
                    and getDistance(objToPoint(action.player), point) > 0,
                    {(x, y) for x in range(0, 4) for y in range(0, 4)},
                )
            )
        elif weapon.type == "rifle":
            targets = {(x, y) for x in range(0, 4) for y in range(0, 4)} - {
                (action.player.x, action.player.y)
            }

        for target in targets:
            if target not in getFreeSpaces(game):
                target_player = game.players.filter(x=target[0], y=target[1]).first()
                if target_player != None:
                    if target_player.hidden:
                        LogEntry(
                            game=game,
                            message=f"{action.player.color} shot {target_player.color}, but missed",
                        ).save()
                        return
                    else:
                        target_player.hp -= 1 if weapon.type != "shotgun" else 2
                        target_player.save()
                        LogEntry(
                            game=game,
                            message=f"{action.player.color} shot {target_player.color}",
                        ).save()
                        return
                else:
                    target_bank = game.banks.filter(x=target[0], y=target[1]).first()
                    if target_bank != None:
                        target_bank.hp -= 10
                        target_bank.save()
                        action.player.dollars += 10
                        action.player.save()
                        LogEntry(
                            game=game,
                            message=f"{action.player.color} robbed a bank and gained 10$",
                        ).save()
                        return
        LogEntry(
            game=game,
            message=f"{action.player.color}'s shot missed",
        ).save()
