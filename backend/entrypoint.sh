#!/usr/bin/bash
# Used for docker compose to apply migrations after the database has been deployed

python manage.py makemigrations &
python manage.py migrate &
python manage.py runserver