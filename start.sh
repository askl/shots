#!/usr/bin/bash

if [ $(systemctl is-active postgresql) != "active" ]
then
    systemctl start postgresql
fi
if [ $(systemctl is-active redis) != "active" ]
then
    systemctl start redis
fi
source backend/venv/bin/activate
pip install --upgrade pip
pip install -r backend/requirements.txt
python backend/manage.py runserver > logs/runserver.log &
npm start --prefix ./frontend > logs/npm_frontend.log &
trap " " SIGINT
tail -f logs/runserver.log logs/npm_frontend.log
trap - SIGINT
deactivate
kill $(pgrep -fo "python.*backend/manage.py")
kill $(pgrep -fo "node.*start.js")