# Shots
###### (Name in progress)
## Project description
The project aims to create a simple, turn-based, strategic browser game set in a wild west setting. The goal of the game is 
to earn the most amount of money and to be placed on the top of the daily/all time leaderboard. The game will be played on a table
containing up to ~6 players, but using in game actions players will be able to change the table in game.

## Rules of the game
Up to 6 players will be placed on a small board containing the player positions and a bank. Each player will start with a certain 
amount of hit points (~5) and money (~0) The goal of the game is to amount the most amount of money and run away which places 
money collected as score on the leaderboard. Players would conduct their actions in a small amount of time (5-10 seconds) by 
selecting them on the menu and their target on the board. <br/>
Player actions actions include:
* Move
	* Changes player position on the board to the one selected. The shots aimed at the old player position miss, while aimed at new player position hit the target.
* Block
  * (Default action if none selected) Performed by moving to the same place the player is currently. Shots aimed at player do not hit.
* Reload
	* Adds 1 ammunition to currently selected wepon.
* Shoot
	* Shoot the other player with the selected wepon. Depending on the wepon may have different range, rules and may consume ammunition. Shooting a bank would earn money.
* Run 
  * If the player has a horse, he escapes with his money and gets placed on the leaderboard. If he doesn't, he will switch his table to a random different one
   	keeping all his items and statistics. If the player gets hit the action is cancelled
<br/>

Outside of their turn players will be able to:
* buy items in a shop. Item list should include
  * healing items
  * different wepon types
  * a horse (allowing the player to escape and get placed on the leaerboard)
*	switch wepons
* use items
* send messages in a chat

## Idea list
* More item types
* Player bounties and wanted points
  * Players would get wanted points for shooting the bank. The more they have he bigger the prize for eliminating them.
  * Bank could contain a finite amount of money and at certain money thresholds could cause events making the game harder.
* AI non player characters
* Train stations
  * At certain positions on the table and at certain turn intervals could allow a players around it to switch a table. (Could not be cancelled)

## Page elements
The pages required to realize the following concept include:
* Login page
  * Where player will be able to set his login and icon.
* Table page
  * The main page where the game will be conducted. It will contain:
    - The board displaying other players and objects on the map
    - Timer till the turn ends
    - Panel containing player statistics and selectable actions
    - Panel displaying shop actions (real time)
    - A chat containing game logs and player messages
* Leaderboard page
  * Containing logins and scores of top players this day/all time.

## Server elements
Server would keep track of currently logged in players, their table states and the leaderboard. 
All the game logic would also be executed there.